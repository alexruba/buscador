#ifndef INDEXADORHASH_H_
#define INDEXADORHASH_H_

#include <iostream>
#include <fstream>
#include <istream>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include "indexadorInformacion.h"
#include "tokenizador.h"
#include <sys/types.h>
#include <sys/stat.h>
#include "stemmer.h"
#include <stdexcept>

using namespace std;

class IndexadorHash {
	friend ostream& operator<<(ostream&, const IndexadorHash&);

	public:
		IndexadorHash(const string&, const string&, const bool&, const bool&,
					  const string&, const int&, const bool&, const bool&);
		IndexadorHash(const string&);
		IndexadorHash(const IndexadorHash&);
		IndexadorHash();
		~IndexadorHash();
		IndexadorHash & operator=(const IndexadorHash&);

		bool Indexar(const string&);
		bool IndexarDirectorio(const string&);
		bool GuardarIndexacion() const;
		bool RecuperarIndexacion(const string&);
		void ImprimirIndexacion() const;
		bool IndexarPregunta(const string&);
		bool DevuelvePregunta(string&) const;
		bool DevuelvePregunta(const string&, InformacionTerminoPregunta&);
		bool DevuelvePregunta(InformacionPregunta&) const;
		void ImprimirIndexacionPregunta();
		void ImprimirPregunta();
		bool Devuelve(const string&, InformacionTermino&) const;
		bool Devuelve(const string&, const string&, InfTermDoc&) const;
		bool Existe(const string&) const;
		bool Borra(const string&);
		bool BorraDoc(const string&);
		void VaciarIndice();
		bool Actualiza(const string&, const InformacionTermino&);
		bool Inserta(const string&, const InformacionTermino&);
		int NumPalIndexadas() const;
		string DevolverFichPalParada() const;
		void ListarPalParada() const;
		int NumPalParada() const;
		string DevolverDelimitadores() const;
		bool DevolverCasosEspeciales() const;
		bool DevolverPasarAminuscSinAcentos() const;
		bool DevolverAlmacenarPosTerm() const;
		Tokenizador DevolverTokenizador() const;
		string DevolverDirIndice() const;
		int DevolverTipoStemming() const;
		bool DevolverAlmEnDisco() const;
		void ListarInfColeccDocs() const;
		void ListarTerminos() const;
		bool ListarTerminos(const string&) const;
		void ListarDocs() const;
		bool ListarDocs(const string&) const;
		void IndexarPalabra(const string&, const string&, int&, int&);
		void IndexarPalabraPregunta(const string&, int&);
		void InformacionTerminoInicialPreg(InformacionTerminoPregunta&, int&);

		unordered_map<string, InformacionTermino> getIndice() const;
		unordered_map<string, InfDoc> getIndiceDocs()const;
		InfColeccionDocs getInformacionColeccionDocs()const;
		string getPregunta()const;
		unordered_map<string, InformacionTerminoPregunta> getIndicePregunta()const;
		InformacionPregunta getInfPregunta()const;
		unordered_set<string> getStopWords()const;
		Tokenizador getTok()const;

	private:
		// Almacenar palabras de parada de fichero de palabras de parada
		void AlmacenarStopWords();

		// Indexación independiente de un documento
		bool IndexarDocumento(const string&, int id);

		// Recuperar informacion del indice
		void RecuperarInformacionIndice(ifstream&);

		// Recuperar informacion del indice de docs
		void RecuperarInformacionIndiceDocs(ifstream& i);

		// Recuperar informacion de palabras de parada
		void RecuperarInformacionStopWords(ifstream& i);

		// Recuperar informacion de datos primitivos de indexador hash
		void RecuperarInformacionDatosPrimitivos(ifstream& i);

		// Recuperar informacion de la pregunta
		void RecuperarPregunta(ifstream& i);

		// Guardar informacion del fichero de stop words
		void GuardarStopWords(ofstream&) const;

		// Guardar datos primitivos de IndexadorHash
		void GuardarDatosPrimitivosIndexador(ofstream&) const;

		// Guardar informacion de indice docs
		void GuardarIndiceDocs(ofstream&) const;

		// Guardar informacion del indice
		void GuardarIndice(ofstream&) const;

		// Guarda informacion de la pregunta
		void GuardarPregunta(ofstream&) const;

		// Índice de términos indexados accesible por el término
		unordered_map<string, InformacionTermino> indice;

		// Índice de documentos indexados accesible por nombre del doc
		unordered_map<string, InfDoc> indiceDocs;

		// Información recogida de la colección de documentos indexada
		InfColeccionDocs informacionColeccionDocs;

		// Pregunta indexada actualmente
		string pregunta;

		// Índice de terminos indexados en una pregunta
		unordered_map<string, InformacionTerminoPregunta> indicePregunta;

		// Información recogida de la pregunta indexada
		InformacionPregunta infPregunta;

		// Palabras de parada
		unordered_set<string> stopWords;

		// Nombre del fichero que contiene las palabras de parada
		string ficheroStopWords;

		//Tokenizador que se usará en la indexación
		Tokenizador tok;

		// Directorio del disco duro donde se almacenará el índice
		string directorioIndice;

		// Tipo de stemmer que se aplicará
		int tipoStemmer;

		// Lugar donde se almacenarán los índices de los documentos
		bool almacenarEnDisco;

		// Almacenar la posición en la que aparecen los términos 
		// dentro del documento en la clase InfTermDoc
		bool almacenarPosTerm;

};

#endif