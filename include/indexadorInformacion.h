#ifndef INDEXADORINFORMACION_H_
#define INDEXADORINFORMACION_H_

#include <iostream>
#include <list>
#include <unordered_map>
#include <time.h>
#include <fstream>

#define ALFABETO_LENGTH 26

using namespace std;


///////////////////////////////////////////////////////////////
//////////////		Node class		///////////////
///////////////////////////////////////////////////////////////

class Node
{
	public:
		Node(char);
		Node();
		~Node();
		char letra;
		bool esPalabra;
		Node* hijos[ALFABETO_LENGTH];
};

///////////////////////////////////////////////////////////////
//////////////		Trie class		///////////////
///////////////////////////////////////////////////////////////


class Trie
{
	public:
		Trie();
		~Trie();
		void anadePalabra(string);
		bool buscaPalabra(string);
		void eliminaPalabra(string);
		Node* getRaiz();

	private:
		Node* raiz;
};

///////////////////////////////////////////////////////////////
//////////////			InfTermDoc class		///////////////
///////////////////////////////////////////////////////////////

class InfTermDoc {
	friend ostream& operator<<(ostream& a, const InfTermDoc& p);
	
	public:
		InfTermDoc(const InfTermDoc &);
		InfTermDoc();
		~InfTermDoc();
		InfTermDoc & operator=(const InfTermDoc &);

		void VaciarPosTerm();
		void Guardar(ofstream&) const;
		void RecuperarInformacion(ifstream&);

		//Getters and setters
		int getFt() const;
		void setFt(int);
		list<int> getPosTerm()const;
		void setPosTerm(list<int>);
		void addPosition(int&);

	private:
		// Frecuencia del término en el documento
		int ft;

		// Lista de números de palabra en los que aparece el término en el documento
		list<int> posTerm;

};


///////////////////////////////////////////////////////////////
//////////////		InfColeccionDocs class		///////////////
///////////////////////////////////////////////////////////////

class InfColeccionDocs {
	friend ostream& operator<<(ostream& s, const InfColeccionDocs& p);

	public:
		InfColeccionDocs(const InfColeccionDocs &);
		InfColeccionDocs();
		~InfColeccionDocs();
		InfColeccionDocs & operator=(const InfColeccionDocs &);

		void Guardar(ofstream&) const;
		void RecuperarInformacion(ifstream&);

		// Getters and setters
		long int getNumDocs() const;
		void setNumDocs(long int);
		long int getNumTotalPal() const;
		void setNumTotalPal(long int);
		long int getNumTotalPalSinParada() const;
		void setNumTotalPalSinParada(long int);
		long int getNumTotalPalDiferentes() const;
		void setNumTotalPalDiferentes(long int);
		long int getTamBytes() const;
		void setTamBytes(long int);

	private:
		// Nº total de documentos de la colección
		long int numDocs;

		// Nº total de palabras en la colección
		long int numTotalPal;

		// Nº total de palabras sin stop-words en la colección
		long int numTotalPalSinParada;

		// Nº total de palabras diferentes en la colección que no sean stop-words
		long int numTotalPalDiferentes;

		// Tamaño total en bytes de la colección
		long int tamBytes;
};


///////////////////////////////////////////////////////////////
//////////////			InfDoc class			///////////////
///////////////////////////////////////////////////////////////

class InfDoc {
	friend ostream& operator<<(ostream& s, const InfDoc& p);

	public:
		InfDoc(const InfDoc&);
		InfDoc();
		~InfDoc();
		InfDoc & operator=(const InfDoc&);

		void Guardar(ofstream&) const;
		void RecuperarInformacion(ifstream&);

		// Getters and setters
		int getIdDoc() const;
		void setIdDoc(int);
		int getNumPal() const;
		void setNumPal();
		int getNumPalSinParada() const;
		void setNumPalSinParada();
		int getNumPalDiferentes() const;
		void setNumPalDiferentes();
		int getTamBytes() const;
		void setTamBytes(int);
		time_t getFecha() const;
		void setFecha(time_t);

	private:
		// Identificador del documento. Empieza por 1
		long int idDoc;

		// Nº total de palabras del documento
		int numPal;

		// Nº total de palabras sin stop-words del documento
		int numPalSinParada;

		// Nº total de palabras diferentes que no sean stop-words
		int numPalDiferentes;

		// Tamaño en bytes del documento
		int tamBytes;

		// Fecha y hora de modificación del documento
		time_t fechaModificacion;
};


///////////////////////////////////////////////////////////////
//////////////		InformacionTermino class 	///////////////
///////////////////////////////////////////////////////////////

class InformacionTermino {
	friend ostream& operator<<(ostream& s, const InformacionTermino& p);

	public:
		InformacionTermino(const InformacionTermino &);
		InformacionTermino();
		~InformacionTermino();
		InformacionTermino & operator=(const InformacionTermino &);

		void Guardar(ofstream&) const;
		void RecuperarInformacion(ifstream&);
		void VaciarLDocs();
		bool addLDocsPosition(int&, int&, bool&);

		// Getters and setters
		int getFtc() const;
		void setFtc(int);
		unordered_map<long int, InfTermDoc> getLDocs() const;
		void setLDocs(unordered_map<long int, InfTermDoc>);
		void BorrarDocumentoID(int&);
		
	private:
		// Frecuencia total del término en la colección
		int ftc;

		// Tabla hash que accederá por el id del doc
		// devolviendo un objeto de la clase InfTermDoc
		// que contiene toda la información de aparición
		// del término en el documento.
		unordered_map<long int, InfTermDoc> l_docs; 
};


///////////////////////////////////////////////////////////////
//////////////	  InformacionPregunta class		///////////////
///////////////////////////////////////////////////////////////

class InformacionPregunta {
	friend ostream& operator<<(ostream&, const InformacionPregunta&);

	public:
		InformacionPregunta(const InformacionPregunta&);
		InformacionPregunta();
		~InformacionPregunta();
		InformacionPregunta & operator=(const InformacionPregunta&);


		//Getters and setters
		long int getNumTotalPal() const;
		void setNumTotalPal(long int);
		long int getNumTotalPalSinParada() const;
		void setNumTotalPalSinParada(long int);
		long int getNumTotalPalDiferentes() const;
		void setNumTotalPalDiferentes(long int);

		void Guardar(ofstream&) const;
		void RecuperarInformacion(ifstream&);

	private:
		// Nº total de palabras en la pregunta
		long int numTotalPal;

		// Nº total de palabras sin stop-words en la pregunta
		long int numTotalPalSinParada;

		// Nº total de palabras diferentes en la pregunta que no sean stop-words
		long int numTotalPalDiferentes;
};


///////////////////////////////////////////////////////////////
//////////////	  InformacionTerminoPregunta 	///////////////
///////////////////////////////////////////////////////////////

class InformacionTerminoPregunta {
	friend ostream& operator<<(ostream&, const InformacionTerminoPregunta&);

	public:
		InformacionTerminoPregunta(const InformacionTerminoPregunta &);
		InformacionTerminoPregunta();
		~InformacionTerminoPregunta();
		InformacionTerminoPregunta & operator=(const InformacionTerminoPregunta &);

		void addPreguntaPosTerm(int&);
		void Guardar(ofstream&)const;
		void RecuperarInformacion(ifstream&);

		//Getters and setters
		int getFt() const;
		void setFt(int ft);
		list<int> getPosTerm() const;
		void setPosTerm(list<int>);

	private:
		// Frecuencia total del término en la pregunta
		int ft;

		// Lista de números de palabra en los que aparece el término en la pregunta
		list<int> posTerm;
};


#endif
