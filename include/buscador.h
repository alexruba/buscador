#ifndef _BUSCADOR_H_
#define _BUSCADOR_H_

#include <iostream>
#include <queue>
#include "indexadorHash.h"

using namespace std;

//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// ResultadoRI class ////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

class ResultadoRI {
	
	friend ostream& operator<<(ostream&, const ResultadoRI&);

	public:
		ResultadoRI(const double&, const long int&, const int&);
		double VSimilitud() const;
		long int IdDoc() const;
		bool operator< (const ResultadoRI&) const;

	private:
		double vSimilitud;
		long int idDoc;
		int numPregunta;

};



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// Buscador class ////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

class Buscador: public IndexadorHash {

	friend ostream& operator<<(ostream&, const Buscador&);

	public:
		Buscador(const string&, const int&);
		Buscador(const Buscador&);
		~Buscador();
		Buscador& operator=(const Buscador&);

		bool Buscar(const int&);
		bool Buscar(const string&, const int&, const int&, const int&);
		void ImprimirResultadoBusqueda(const int&) const;

		bool ImprimirResultadoBusqueda(const int&, const string&) const;
		int DevolverFormulaSimilitud() const;
		bool CambiarFormulaSimilitud(const int&);
		void CambiarParametrosDFR(const double&);
		double DevolverParametrosDFR() const;
		void CambiarParametrosBM25(const double&, const double&);
		void DevolverParametrosBM25(double&, double&) const;

	private:
		Buscador();
		priority_queue<ResultadoRI> docsOrdenados;
		int formSimilitud;
		double c;
		double k1;
		double b;

};

#endif

