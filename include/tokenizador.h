#ifndef TOKENIZADOR_H_
#define TOKENIZADOR_H_

#include <string>
#include <iostream>
#include <list>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <stdlib.h>
#include <cwchar>
#include <vector>

using namespace std;

class Tokenizador {
	friend ostream& operator<<(ostream&, const Tokenizador &);

public:
	Tokenizador(const string&, const bool&, const bool&);
	Tokenizador(const Tokenizador&);
	Tokenizador();
	~Tokenizador();

	Tokenizador& operator=(const Tokenizador&);

	void CasosEspeciales(const bool&);
	bool CasosEspeciales() const;
	void PasarAminuscSinAcentos(const bool&);
	bool PasarAminuscSinAcentos() const;
	string PasarAMinusculasSinAcentos(const string&) const; 

	bool isNumber(string&, string::size_type&, string::size_type&, int&, const string&) const;

	void Tokenizar(const string&, ofstream&) const;
	bool Tokenizar(const string&, const string&) const;
	bool Tokenizar(const string&) const;

	bool TokenizarCasoEspecialAcronimo(string&, string::size_type&, string::size_type&, int&, const string&) const;
	void TokenizarCasoEspecialGuion(string&, string::size_type&, string::size_type&, const string&) const;
	void TokenizarCasoEspecialDecimal(string&, string::size_type&, string::size_type&, int&, const string&) const;
	void TokenizarCasoEspecialEmail(string&, string::size_type&, string::size_type&, const string&) const;
	void TokenizarCasoEspecialURL(string&, string::size_type&, string::size_type&, const string&) const;

	void TokenizarSinCasosEspeciales(const string&, ofstream&) const;
	bool TokenizarListaIntermedia(const string& str, list<string>&) const;
	
	void TokenizarEnLista(const string& str, list<string>&) const;
	bool TokenizarListaFicheros(const string&) const;
	bool TokenizarDirectorio(const string&) const;

	void TokenizarPregunta(const string&, vector<string>&);
	
	void DelimitadoresPalabra(const string&);
	void AnyadirDelimitadoresPalabra(const string&);
	string DelimitadoresPalabra() const;

	void Guardar(ofstream&) const;
	void RecuperarInformacion(ifstream&);

private:

	string delimiters;
	bool casosEspeciales;
	bool pasarAminuscSinAcentos;
};


#endif
