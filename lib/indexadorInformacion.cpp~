#include "indexadorInformacion.h"

///////////////////////////////////////////////////////////////
//////////////		InfColeccionDocs class		///////////////
///////////////////////////////////////////////////////////////

ostream& operator<<(ostream& s, const InfColeccionDocs& p)
{
	s << "numDocs: " << p.numDocs << 
		 "\tnumTotalPal: " << p.numTotalPal <<
		 "\tnumTotalPalSinParada: " << p.numTotalPalSinParada << 
		 "\tnumTotalPalDiferentes: " << p.numTotalPalDiferentes << 
		 "\ttamBytes: " << p.tamBytes;

	return s;
}

InfColeccionDocs::InfColeccionDocs(const InfColeccionDocs &infColeccionDocs)
{
	numDocs = infColeccionDocs.getNumDocs();
	numTotalPal = infColeccionDocs.getNumTotalPal();
	numTotalPalSinParada = infColeccionDocs.getNumTotalPalSinParada();
	numTotalPalDiferentes = infColeccionDocs.getNumTotalPalDiferentes();
	tamBytes = infColeccionDocs.getTamBytes();
}

InfColeccionDocs::InfColeccionDocs()
{
	numDocs = 0;
	numTotalPal = 0;
	numTotalPalSinParada = 0;
	numTotalPalDiferentes = 0;
	tamBytes = 0;
}

InfColeccionDocs::~InfColeccionDocs()
{
	numDocs = 0;
	numTotalPal = 0;
	numTotalPalSinParada = 0;
	numTotalPalDiferentes = 0;
	tamBytes = 0;
}

InfColeccionDocs& InfColeccionDocs::operator=(const InfColeccionDocs& infColeccionDocs)
{
	if(this != &infColeccionDocs)
	{
		this->~InfColeccionDocs();
		numDocs = infColeccionDocs.getNumDocs();
		numTotalPal = infColeccionDocs.getNumTotalPal();
		numTotalPalSinParada = infColeccionDocs.getNumTotalPalSinParada();
		numTotalPalDiferentes = infColeccionDocs.getNumTotalPalDiferentes();
		tamBytes = infColeccionDocs.getTamBytes();
	}

	return *this;
}

void InfColeccionDocs::Guardar(ofstream& i) const
{
	i << numDocs << "\n";
	i << numTotalPal << "\n";
	i << numTotalPalSinParada << "\n";
	i << numTotalPalDiferentes << "\n";
	i << tamBytes << "\n";
}

void InfColeccionDocs::RecuperarInformacion(ifstream& i)
{
	i >> numDocs;
	i >> numTotalPal;
	i >> numTotalPalSinParada;
	i >> numTotalPalDiferentes;
	i >> tamBytes;
}

// Getters and setters

long int InfColeccionDocs::getNumDocs() const{ return numDocs; }
void InfColeccionDocs::setNumDocs(long int numDocs){ this->numDocs = numDocs; }
long int InfColeccionDocs::getNumTotalPal() const{ return numTotalPal; }
void InfColeccionDocs::setNumTotalPal(long int numTotalPal){ this->numTotalPal = numTotalPal; }
long int InfColeccionDocs::getNumTotalPalSinParada() const{ return numTotalPalSinParada; } 
void InfColeccionDocs::setNumTotalPalSinParada(long int numTotalPalSinParada){ this->numTotalPalSinParada = numTotalPalSinParada; }
long int InfColeccionDocs::getNumTotalPalDiferentes() const{ return numTotalPalDiferentes; }
void InfColeccionDocs::setNumTotalPalDiferentes(long int numTotalPalDiferentes){ this->numTotalPalDiferentes = numTotalPalDiferentes; }
long int InfColeccionDocs::getTamBytes() const{ return tamBytes; }
void InfColeccionDocs::setTamBytes(long int tamBytes){ this->tamBytes = tamBytes; }


///////////////////////////////////////////////////////////////
//////////////			InfDoc class			///////////////
///////////////////////////////////////////////////////////////


ostream& operator<<(ostream& s, const InfDoc& p)
{
	s << "idDoc: " << p.idDoc <<
		 "\tnumPal: " << p.numPal << 
		 "\tnumPalSinParada: " << p.numPalSinParada <<
		 "\tnumPalDiferentes: " << p.numPalDiferentes <<
		 "\ttamBytes: " << p.tamBytes;

	return s;
}

InfDoc::InfDoc(const InfDoc& infDoc)
{
	idDoc = infDoc.getIdDoc();
	numPal = infDoc.getNumPal();
	numPalSinParada = infDoc.getNumPalSinParada();
	numPalDiferentes = infDoc.getNumPalDiferentes();
	tamBytes = infDoc.getTamBytes();
	fechaModificacion = infDoc.getFecha();
}

InfDoc::InfDoc()
{
	idDoc = 0;
	numPal = 0;
	numPalSinParada = 0;
	numPalDiferentes = 0;
	tamBytes = 0;
	fechaModificacion = 0;
}

InfDoc::~InfDoc()
{
	idDoc = 0;
	numPal = 0;
	numPalSinParada = 0;
	numPalDiferentes = 0;
	tamBytes = 0;
	fechaModificacion = 0;
}

InfDoc& InfDoc::operator=(const InfDoc& infDoc)
{
	if(this != &infDoc)
	{
		this->~InfDoc();
		idDoc = infDoc.getIdDoc();
		numPal = infDoc.getNumPal();
		numPalSinParada = infDoc.getNumPalSinParada();
		numPalDiferentes = infDoc.getNumPalDiferentes();
		tamBytes = infDoc.getTamBytes();
		fechaModificacion = infDoc.getFecha();
	}

	return *this;
}

void InfDoc::Guardar(ofstream &i) const
{
	i << idDoc << "\n";
	i << numPal << "\n";
	i << numPalSinParada << "\n";
	i << numPalDiferentes << "\n";
	i << tamBytes << "\n";
	i << fechaModificacion << "\n";
}

void InfDoc::RecuperarInformacion(ifstream& i)
{
	i >> idDoc;
	i >> numPal;
	i >> numPalSinParada;
	i >> numPalDiferentes;
	i >> tamBytes;
	i >> fechaModificacion;
}

// Getters and setters
int InfDoc::getIdDoc() const{ return idDoc; }
void InfDoc::setIdDoc(int idDoc){ this->idDoc = idDoc; }
int InfDoc::getNumPal() const{ return numPal; }
void InfDoc::setNumPal(){ numPal++; }
int InfDoc::getNumPalSinParada() const{ return numPalSinParada; } 
void InfDoc::setNumPalSinParada(){ numPalSinParada++; }
int InfDoc::getNumPalDiferentes() const{ return numPalDiferentes; }
void InfDoc::setNumPalDiferentes(){ numPalDiferentes++; }
int InfDoc::getTamBytes() const{ return tamBytes; }
void InfDoc::setTamBytes(int tamBytes){ this->tamBytes = tamBytes; }
time_t InfDoc::getFecha() const{ return fechaModificacion; }
void InfDoc::setFecha(time_t fechaModificacion){ this->fechaModificacion = fechaModificacion; }



///////////////////////////////////////////////////////////////
//////////////		InformacionTermino class 	///////////////
///////////////////////////////////////////////////////////////

ostream& operator<<(ostream& s, const InformacionTermino& p)
{
	s << "Frecuencia total: " << p.ftc <<
		 "\tfd: " << p.l_docs.size();

	for ( auto it = p.l_docs.begin(); it != p.l_docs.end(); ++it )
		s << "\tId.Doc: " << it->first << "\t" << it->second;

	return s;
}

InformacionTermino::InformacionTermino(const InformacionTermino &infTerm)
{
	ftc = infTerm.ftc;
	l_docs = infTerm.l_docs;
}

InformacionTermino::InformacionTermino()
{
	ftc = 0;
	l_docs.clear();
}

InformacionTermino::~InformacionTermino()
{
	ftc = 0;
	l_docs.clear();
}

void InformacionTermino::VaciarLDocs()
{
	l_docs.clear();
}

InformacionTermino & InformacionTermino::operator=(const InformacionTermino& infTerm)
{
	if(this != &infTerm)
	{
		ftc = infTerm.getFtc();
		l_docs = infTerm.getLDocs();
	}

	return *this;
}

void InformacionTermino::Guardar(ofstream &i) const
{
	i << ftc << "\n";

	i << l_docs.size() << "\n";

	auto it = l_docs.begin();
	while(it != l_docs.end())
	{
		i << it->first << "\n";
		it->second.Guardar(i);
		++it;
	}
}

void InformacionTermino::RecuperarInformacion(ifstream& i)
{
	i >> ftc;

	int doc;
	InfTermDoc info;

	int size;
	i >> size;

	for(int j = 0; j < size; ++j)
	{
		i >> doc;
		info.RecuperarInformacion(i);
		l_docs[doc] = info;
		info.VaciarPosTerm();
	}
}

// Getters and setters
int InformacionTermino::getFtc() const{ return ftc; }
void InformacionTermino::setFtc(int ftc){ this->ftc = ftc; }
unordered_map<long int, InfTermDoc> InformacionTermino::getLDocs() const{ return l_docs; }
void InformacionTermino::setLDocs(unordered_map<long int, InfTermDoc> l_docs){ this->l_docs = l_docs; }

bool InformacionTermino::addLDocsPosition(int& id, int& pos, bool& almacenarPosTerm)
{

	l_docs[id].setFt(l_docs[id].getFt()+1);
	if(almacenarPosTerm) l_docs[id].addPosition(pos);

	return l_docs[id].getFt() == 1;
}


void InformacionTermino::BorrarDocumentoID(int& id)
{
	this->setFtc(this->getFtc() - this->getLDocs().at(id).getFt());
	l_docs.erase(id);
}


///////////////////////////////////////////////////////////////
//////////////	  InformacionPregunta class		///////////////
///////////////////////////////////////////////////////////////

ostream& operator<<(ostream& s, const InformacionPregunta& p)
{
	s << "numTotalPal: " << p.numTotalPal << 
		 "\tnumTotalPalSinParada: " << p.numTotalPalSinParada <<
		 "\tnumTotalPalDiferentes: " << p.numTotalPalDiferentes;

	return s;
}

InformacionPregunta::InformacionPregunta(const InformacionPregunta& infPregunta)
{
	numTotalPal = infPregunta.getNumTotalPal();
	numTotalPalSinParada = infPregunta.getNumTotalPalSinParada();
	numTotalPalDiferentes = infPregunta.getNumTotalPalDiferentes();
}

InformacionPregunta::InformacionPregunta()
{
	numTotalPal = 0;
	numTotalPalSinParada = 0;
	numTotalPalDiferentes = 0;
}

InformacionPregunta::~InformacionPregunta()
{
	numTotalPal = 0;
	numTotalPalSinParada = 0;
	numTotalPalDiferentes = 0;
}

InformacionPregunta& InformacionPregunta::operator=(const InformacionPregunta& infPregunta)
{
	if(this != &infPregunta)
	{
		this->~InformacionPregunta();
		numTotalPal = infPregunta.getNumTotalPal();
		numTotalPalSinParada = infPregunta.getNumTotalPalSinParada();
		numTotalPalDiferentes = infPregunta.getNumTotalPalDiferentes();
	}

	return *this;
}

void InformacionPregunta::Guardar(ofstream& i) const
{
	i << numTotalPal << "\n";
	i << numTotalPalSinParada << "\n";
	i << numTotalPalDiferentes << "\n";
}

void InformacionPregunta::RecuperarInformacion(ifstream& i)
{
	i >> numTotalPal;
	i >> numTotalPalSinParada;
	i >> numTotalPalDiferentes;
}

//Getters and setters
long int InformacionPregunta::getNumTotalPal() const{ return numTotalPal; }
void InformacionPregunta::setNumTotalPal(long int numTotalPal){ this->numTotalPal = numTotalPal; }
long int InformacionPregunta::getNumTotalPalSinParada() const{ return numTotalPalSinParada; } 
void InformacionPregunta::setNumTotalPalSinParada(long int numTotalPalSinParada){ this->numTotalPalSinParada = numTotalPalSinParada; }
long int InformacionPregunta::getNumTotalPalDiferentes() const{ return numTotalPalDiferentes; }
void InformacionPregunta::setNumTotalPalDiferentes(long int numTotalPalDiferentes){ this->numTotalPalDiferentes = numTotalPalDiferentes; }


///////////////////////////////////////////////////////////////
//////////////	  InformacionTerminoPregunta 	///////////////
///////////////////////////////////////////////////////////////

ostream& operator<<(ostream& s, const InformacionTerminoPregunta& p)
{
	s << "ft: " << p.ft;
	for(auto it = p.posTerm.begin(); it != p.posTerm.end(); ++it)
		cout << '\t' <<  *it;
	return s;
}

InformacionTerminoPregunta::InformacionTerminoPregunta(const InformacionTerminoPregunta& infTermPregunta)
{
	ft = infTermPregunta.ft;
	posTerm = infTermPregunta.posTerm;
}

InformacionTerminoPregunta::InformacionTerminoPregunta()
{
	ft = 0;
	posTerm.clear();
}

InformacionTerminoPregunta::~InformacionTerminoPregunta()
{
	ft = 0;
	posTerm.clear();
}

InformacionTerminoPregunta& InformacionTerminoPregunta::operator=(const InformacionTerminoPregunta& infTermPregunta)
{
	if(this != &infTermPregunta)
	{
		this->~InformacionTerminoPregunta();
		ft = infTermPregunta.getFt();
		posTerm = infTermPregunta.getPosTerm();
	}

	return *this;
}

void InformacionTerminoPregunta::addPreguntaPosTerm(int& pos) { this->posTerm.push_back(pos); }

void InformacionTerminoPregunta::Guardar(ofstream& i)const
{
	i << ft << "\n";
	i << posTerm.size() << "\n";

	for(auto it = posTerm.begin(); it != posTerm.end(); ++it)
		i << *it << "\n";
}

void InformacionTerminoPregunta::RecuperarInformacion(ifstream& i)
{
	posTerm.clear();

	i >> ft;
	int size;
	i >> size;

	int pos;


	for(int j = 0; j < size; ++j)
	{
		i >> pos;
		posTerm.push_back(pos);
	}
}

// Getters and setters
int InformacionTerminoPregunta::getFt() const{ return ft; }
void InformacionTerminoPregunta::setFt(int ft){ this->ft = ft; }
list<int> InformacionTerminoPregunta::getPosTerm() const{ return posTerm; }
void InformacionTerminoPregunta::setPosTerm(list<int>){ this->posTerm = posTerm; }


///////////////////////////////////////////////////////////////
//////////////			InfTermDoc class		///////////////
///////////////////////////////////////////////////////////////

ostream& operator<<(ostream& s, const InfTermDoc& p)
{
	s << "ft: " << p.ft;

	for(auto it = p.posTerm.begin(); it != p.posTerm.end(); ++it)
		s << "\t" << *it;

	return s;
}

InfTermDoc::InfTermDoc(const InfTermDoc &infTermDoc)
{
	ft = infTermDoc.ft;
	posTerm = infTermDoc.posTerm;
}

InfTermDoc::InfTermDoc()
{
	ft = 0;
	posTerm.clear();
}

InfTermDoc::~InfTermDoc()
{
	ft = 0;
	posTerm.clear();
}

InfTermDoc& InfTermDoc::operator=(const InfTermDoc &infTermDoc)
{
	if(this != &infTermDoc)
	{
		this->~InfTermDoc();
		ft = infTermDoc.getFt();
		posTerm = infTermDoc.getPosTerm();
	}

	return *this;
}

void InfTermDoc::VaciarPosTerm()
{
	posTerm.clear();
}

void InfTermDoc::Guardar(ofstream& i) const
{
	i << ft << "\n";

	i << posTerm.size() << "\n";
	for(int pos: posTerm)
		i << pos << "\n";
}

void InfTermDoc::RecuperarInformacion(ifstream& i)
{
	i >> ft;

	int pos;
	int size;
	i >> size;
	for(int j = 0; j < size; ++j)
	{
		i >> pos;
		posTerm.push_back(pos);
	}
}

// Getters and setters
int InfTermDoc::getFt() const{ return ft; }
void InfTermDoc::setFt(int ft){ this->ft = ft; }
list<int> InfTermDoc::getPosTerm()const{ return posTerm; }
void InfTermDoc::setPosTerm(list<int>posTerm){ this->posTerm = posTerm; }
void InfTermDoc::addPosition(int& pos){ this->posTerm.push_back(pos); }

///////////////////////////////////////////////////////////////
//////////////			Node class		///////////////
///////////////////////////////////////////////////////////////

Node::Node()
{
	for(int i = 0; i < ALFABETO_LENGTH; i++)
		hijos[i] = NULL;
}

Node::Node(char letra)
{
	this->letra = letra;
	for(int i = 0;  i < ALFABETO_LENGTH; i++)
		hijos[i] = NULL;
}

Node::~Node()
{
	
}

///////////////////////////////////////////////////////////////
//////////////			Trie class		///////////////
///////////////////////////////////////////////////////////////



