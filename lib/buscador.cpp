#include "buscador.h"

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// Buscador class ////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

ostream& operator<<(ostream& s, const Buscador& p)
{
	string preg;
	s << "Buscador: " << endl;
	if(p.DevuelvePregunta(preg))
		s << "\tPregunta indexada: " << preg << endl;
	else
		s << "\tNo hay ninguna pregunta indexada" << endl;

	s << "\tDatos del indexador: " << endl << (IndexadorHash) p;

	return s;
}

Buscador::Buscador(const string& directorioIndexacion, const int& f):
			IndexadorHash(directorioIndexacion)
{
	formSimilitud = f;
	c= 2;
	k1 = 1.2;
	b = 0.75;
}

Buscador::Buscador(const Buscador& busc) : IndexadorHash(busc)
{
	formSimilitud = busc.formSimilitud;
	c = busc.c;
	k1 = busc.k1;
	b = busc.b;
}

Buscador::~Buscador(){ }

Buscador& Buscador::operator=(const Buscador& busc)
{
	formSimilitud = busc.formSimilitud;
	c = busc.c;
	k1 = busc.k1;
	b = busc.b;
}

int Buscador::DevolverFormulaSimilitud() const{ return formSimilitud; }

bool Buscador::CambiarFormulaSimilitud(const int& f)
{
	if(f == 0 || f== 1)
	{
		formSimilitud = f;
		return true;
	}
	else
		return false;

}

void Buscador::CambiarParametrosDFR(const double& kc){ c = kc; }
double Buscador::DevolverParametrosDFR() const{ return c; }
void Buscador::CambiarParametrosBM25(const double& kk1, const double& kb){ k1 = kk1; b = kb; }
void Buscador::DevolverParametrosBM25(double& kk1, double& kb) const{ kk1 = k1; kb = b; }

//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// ResultadoRI class ////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

ResultadoRI::ResultadoRI(const double& kvSimilitud, const long int& kidDoc, const int& np)
{
	vSimilitud = kvSimilitud;
	idDoc = kidDoc;
	numPregunta = np;
}

double ResultadoRI::VSimilitud() const { return vSimilitud; }
long int ResultadoRI::IdDoc() const{ return idDoc; }

bool ResultadoRI::operator<(const ResultadoRI& lhs) const
{
	if(numPregunta == lhs.numPregunta)
		return (vSimilitud < lhs.vSimilitud);
	else
		return (numPregunta > lhs.numPregunta);
}

ostream& operator<<(ostream& os, const ResultadoRI &res)
{
	os << res.vSimilitud << "\t\t" << res.idDoc << "\t" << res.numPregunta << endl;
	return os;
}

