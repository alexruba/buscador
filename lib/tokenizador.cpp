#include "tokenizador.h"

// Operador salida
ostream& operator<<(ostream& os, const Tokenizador& tokenizador)
{
	os << "DELIMITADORES: " + tokenizador.DelimitadoresPalabra() <<
	" TRATA CASOS ESPECIALES: " << tokenizador.CasosEspeciales() <<
	" PASAR A MINUSCULAS Y SIN ACENTOS: " << tokenizador.PasarAminuscSinAcentos();

	return os;
}

// Constructor a partir de delimitadores, si se detectan palabras compuestas y si se pasa a minusculas
Tokenizador::Tokenizador(const string& delimitadoresPalabra, const bool& detectComp, const bool& minuscSinAcentos)
{
	delimiters = delimitadoresPalabra;
	casosEspeciales = detectComp;
	pasarAminuscSinAcentos = minuscSinAcentos;
}

// Constructor copia
Tokenizador::Tokenizador(const Tokenizador& tokenizador)
{
	if(tokenizador.DelimitadoresPalabra() != "")
		delimiters = tokenizador.DelimitadoresPalabra();
	casosEspeciales = tokenizador.CasosEspeciales();
	pasarAminuscSinAcentos = tokenizador.PasarAminuscSinAcentos();
}

// Constructor por defecto
Tokenizador::Tokenizador()
{
	delimiters=",;:.-/+*\\ '’\"{}[]()<>¡!¿?&#=·•\t\n\r@";
	casosEspeciales = true;
	pasarAminuscSinAcentos = false;
}

// Destructor
Tokenizador::~Tokenizador()
{
	delimiters = "";
	casosEspeciales = true;
	pasarAminuscSinAcentos = false;
}

// Operador =
Tokenizador& Tokenizador::operator=(const Tokenizador& tokenizador)
{
	if(this != &tokenizador)
	{
		this->~Tokenizador();
		delimiters = tokenizador.DelimitadoresPalabra();
		casosEspeciales = tokenizador.CasosEspeciales();
		pasarAminuscSinAcentos = tokenizador.PasarAminuscSinAcentos();
	}

	return *this;
}

// Setter CasosEspeciales
void Tokenizador::CasosEspeciales(const bool& nuevoCasosEspeciales)
{
	casosEspeciales = nuevoCasosEspeciales;
}

// Getter CasosEspeciales
bool Tokenizador::CasosEspeciales() const
{
	return casosEspeciales;
}

// Setter PasarAminuscSinAcentos
void Tokenizador::PasarAminuscSinAcentos(const bool& nuevoPasarAminuscSinAcentos)
{
	pasarAminuscSinAcentos = nuevoPasarAminuscSinAcentos;
}

// Getter PasarAminuscSinAcentos
bool Tokenizador::PasarAminuscSinAcentos() const
{
	return pasarAminuscSinAcentos;
}

// Cambia “delimiters” por “nuevoDelimiters”
void Tokenizador::DelimitadoresPalabra(const string& nuevoDelimiters)
{
	delimiters = nuevoDelimiters;
}

// Anade al final de “delimiters” los nuevos delimitadores que aparezcan
// en “nuevoDelimiters” (no se almacenaran caracteres repetidos)
void Tokenizador::AnyadirDelimitadoresPalabra(const string& nuevoDelimiters)
{
	for(int i = 0; i < nuevoDelimiters.length(); i++)
	{
		size_t found = delimiters.find(nuevoDelimiters.at(i));
		if(found == string::npos)
			delimiters += nuevoDelimiters.at(i);
	}
}

// Devuelve “delimiters”
string Tokenizador::DelimitadoresPalabra() const
{
	return delimiters;
}

// Tokenizamos la cadena str, almacenando los tokens en una lista
void Tokenizador::Tokenizar(const string& str, ofstream& f) const
{	
	string delimitadores = delimiters;
	string cadena = str;

	// Si pasarAminuscSinAcentos esta a true, pasaremos a minusculas
	// y eliminaremos los acentos
	if(pasarAminuscSinAcentos)
	{
		cadena = PasarAMinusculasSinAcentos(str);
	}

	// Si casosEspeciales esta a true, tokenizaremos a partir de las heuristicas
	if(casosEspeciales)
	{
		bool puntoDelimitador = true;
		bool leave = false;
		bool tokeniza = true;
		bool number = true;
		int length = str.length();
		int tokenLength;
		string token = "";
		string caracter = "";

		// Si no esta el espacio entre los delimitadores, lo incluimos
		size_t found = delimitadores.find(" ");
		if(found == string::npos)
			delimitadores += " ";
		
		// Buscamos el primer delimitador a partir del primer no-delimitador
		string::size_type lastPos = cadena.find_first_not_of(delimitadores, 0);
		string::size_type pos = cadena.find_first_of(delimitadores, lastPos);
		string::size_type initialPos = lastPos;

		// Si no esta el punto como delimitador, lo anadimos para detectar sus
		// casos especiales e indicamos puntoDelimitador como false si no esta
		// como delimitador
		found = delimitadores.find(".");
		if(found == string::npos)
		{
			delimitadores += ".";
			puntoDelimitador = false;
		}
		
		// Mientras tengamos caracteres suficientes en la linea como para realizar
		// otra iteracion de busqueda de delimitadores y no-delimitadores
		while(string::npos != pos || string::npos != lastPos)
		{
			// Comprueba si una cadena se ha tokenizado por parte de los acronimos
			tokeniza = true;
			// Comprueba si es un numero la cadena que estudiaremos mas adelante
			number = true;

			// Si se detectan una h o una f puede que estemos cerca de un caso de URL
			if(cadena[lastPos] == 'h' || cadena[lastPos] == 'f')
			{
				// Si el segundo caracter es una t, entonces ya comprobamos si es un
				// caso de URL
				if(cadena[lastPos+1] == 't')
				{
						if(cadena.substr(lastPos, lastPos+2) == "http:")
							TokenizarCasoEspecialURL(cadena, initialPos, pos, delimitadores);
						else if(cadena.substr(lastPos, lastPos+3) == "https:")
							TokenizarCasoEspecialURL(cadena, initialPos, pos, delimitadores);
						else if(cadena.substr(lastPos, lastPos+1) == "ftp:")
							TokenizarCasoEspecialURL(cadena, initialPos, pos, delimitadores);
				}
			
			}

			// Si el valor encontrado es un @ y los valores a la derecha e izquierda son numeros o letras
			// estaremos en un caso de email y tokenizaremos ese caso especial
			if(string::npos != pos && cadena.at(pos) == '@' &&
			  ((isdigit((int)cadena.at(pos-1)) || isalpha((int)cadena.at(pos-1))) &&
			  (isdigit((int)cadena.at(pos+1)) || isalpha((int)cadena.at(pos+1)))))
				TokenizarCasoEspecialEmail(cadena, initialPos, pos, delimitadores);
			
			// Si el valor encontrado es una , y los valores tanto a la derecha como izquierda son numeros
			// comprobamos si es numero el token que extraemos hasta el siguiente delimitador que no sea 
			// un numero o signo ńumerico.
			// Entonces, tokenizamos el caso especial para los numeros decimales
			if(string::npos != pos && cadena.at(pos) == ',' && pos + 1 < length &&
			   isdigit((int)cadena.at(pos-1)) && isdigit((int)cadena.at(pos+1)) && isNumber(cadena, initialPos, pos, length, delimitadores))
				TokenizarCasoEspecialDecimal(cadena, initialPos, pos, length, delimitadores);

			// Si el valor encontrado es un guion, tokenizaremos su caso especial
			if(string::npos != pos && cadena.at(pos) == '-')
				TokenizarCasoEspecialGuion(cadena, initialPos, pos, delimitadores);

			// Si el valor encontrado es un punto, tokenizamos
			if(string::npos != pos && cadena.at(pos) == '.')
				tokeniza = TokenizarCasoEspecialAcronimo(cadena, initialPos, pos, length, delimitadores);
			
			// Si el valor encontrado no era un punto, comprobamos si tenemos un digito suelto con un
			// punto o coma delante para anadirle el 0 y el signo correspondiente (normalizacion)
			else if(string::npos != lastPos && isdigit((int)cadena.at(lastPos)) && lastPos-1 < length &&
				   (cadena.at(lastPos-1) == '.' || cadena.at(lastPos-1) == ','))
			{
				// En el caso de que sea punto, anadimos el punto y el 0
				if(cadena.at(lastPos-1) == '.')
				{
					cadena.insert(lastPos, "0");
					cadena.insert(lastPos+1, ".");
					initialPos = cadena.find_first_not_of(delimitadores, pos);
					pos = cadena.find_first_of(delimitadores, initialPos);
				}
				// En el caso de que sea coma, comprobamos, si existe el valor de la cadena
				// en lastPos-2, si no es un digito, si no es asi, insertamos 0 y coma
				// Este caso es para evitar anadir 0 y coma en caso de detectar un caracter
				// que no sea numero a la hora de tokenizar el numero decimal
				else if(cadena.at(lastPos-1) == ',' && lastPos-2 != string::npos && !isdigit((int) cadena.at(lastPos-2)))
				{
					cadena.insert(lastPos, "0");
					cadena.insert(lastPos+1, ",");
					initialPos = cadena.find_first_not_of(delimitadores, pos);
					pos = cadena.find_first_of(delimitadores, initialPos);
				}
			}

			// Ya establecidas las heuristicas, tokenizamos
			token = cadena.substr(lastPos, pos - lastPos);
			tokenLength = token.length();

			// Si el valor ultimo del token no es uno de los valores siguientes, tokenizamos normal
			if(token[tokenLength-1] != '%' && token[tokenLength-1] != '$' && token[tokenLength-1] != -86 && token[tokenLength-1] != -70 && token[tokenLength-1] != -84)
			{
				// Evitamos el caso de eliminar el punto del final si no era delimitador
				if(!tokeniza && !puntoDelimitador)
					f << token << "." << "\n";
				else
					f << token << "\n";
			}
			else
			{
				// Si en otro caso, es alguno de los siguientes valores
				if(token[tokenLength-1] == '%' || token[tokenLength-1] == '$')
				{
					// Comprobamos que sea un numero correcto
					for(string::iterator it = token.begin(); it != token.end()-1 && number; ++it)
					{
						if(!isdigit((int)*it) && *it != ',' && *it!= '.')
							number = false;
					}
					// Si es numero, tokenizamos separando el ultimo caracter del numero
					if(number)
					{
						caracter = token[tokenLength-1];
						f << token.substr(0,tokenLength-1) << "\n";
						f << caracter << "\n";
					}
					// Sino tokenizamos normal
					else
						f << token << "\n";
				}
				// Si detectamos uno de los valores -> "ºª"
				else if(token[tokenLength-1] == -62)
				{
					// Comprobamos que sea un numero
					for(string::iterator it = token.begin(); it != token.end()-3 && number; ++it)
					{
						if(!isdigit((int)*it) && *it != ',' && *it!= '.')
							number = false;
					}
					// Si no es numero, separamos segun el caracter que nos hayamos encontrado y 
					// lo separamos del numero
					if(number)
					{
						f << token.substr(0,tokenLength-2) << "\n";
						if(token[tokenLength-1] == -86)
							f << "ª" << "\n";
						else
							f << "º" << "\n";
					}
					// Sino tokenizamos normal
					else
						f << token << "\n";
				}
				// Si alcanzamos un caracter ultimo como el euro
				else if(token[tokenLength-1] == -84)
				{
					// Comprobamos que sea numero
					for(string::iterator it = token.begin(); it != token.end()-4 && number; ++it)
					{
						if(!isdigit((int)*it) && *it != ',' && *it!= '.')
							number = false;
					}
					// Si es numero separamos el numero del euro
					if(number)
					{
						f << token.substr(0,tokenLength-3) << "\n";
						f << "€" << "\n";
					}
					// Sino tokenizamos normal
					else
						f << token << "\n";
				}
			}
			
			// Una vez tokenizado, seguimos buscando no-delimitadores
			lastPos = cadena.find_first_not_of(delimitadores, pos);

			// Evitamos el caso de eliminar el primer punto cuando no era delimitador el punto
			if(!puntoDelimitador && string::npos != lastPos-1 && string::npos != lastPos && string::npos != lastPos+1 && cadena.at(lastPos-1) == '.' && cadena.at(lastPos+1) != '.')
				lastPos = lastPos-1;

			// Buscamos el siguiente delimitador a partir del primer no delimitador encontrado
			pos = cadena.find_first_of(delimitadores, lastPos);
		}
	}
	// Sino, tokenizamos sin casos especiales
	else 
		TokenizarSinCasosEspeciales(cadena, f);
}

// Comprueba a partir de los delimitadores y la cadena que se ha pasado
// Si la palabra encontrada (final con delimitador) es un numero
bool Tokenizador::isNumber(string& cadena, string::size_type& initialPos,
						   string::size_type& pos, int& length, const string& delimitadores) const
{
	bool number = true;

	if(string::npos != pos+1)
	{
		for(string::iterator it = cadena.begin()+pos+1; it != cadena.end() && delimitadores.find(*it) == string::npos && number; ++it)
		{
			if(!isdigit((int)*it) && *it != '.' && *it != ',' && *it != '%' && *it != '$' && *it>0)
				number = false;
		}
	}

	return number;
}


// Tokeniza el caso especial de los numeros decimales, en esta practica se tomara
// tanto numeros decimales como todos aquellos numeros que incluyan comas, sean o 
// no indicadoras de numero decimal o si el punto va delante o detras de la coma
// por si tratamos con miles
void Tokenizador::TokenizarCasoEspecialDecimal(string& cadena, string::size_type& initialPos,
											   string::size_type& pos, int& length, const string& delimitadores) const
{
	while(string::npos != pos && cadena.at(pos) == ',' && pos + 1 < length-1 &&
		  isdigit((int)cadena.at(pos-1)) && isdigit((int)cadena.at(pos+1)))
	{
		initialPos = cadena.find_first_not_of(delimitadores, pos);
		pos = cadena.find_first_of(delimitadores, initialPos);
	}

	if(string::npos != pos && cadena.at(pos) == '.') 
		TokenizarCasoEspecialAcronimo(cadena, initialPos, pos, length, delimitadores);
}

// Tokeniza el caso especial guion, iterando a traves de la palabra encontrado guiones
// y seguir avanzando hasta que se encuentre un delimitador que no sea guion
void Tokenizador::TokenizarCasoEspecialGuion(string& cadena, string::size_type& initialPos,
											 string::size_type& pos, const string& delimitadores) const
{
	while(string::npos != pos && cadena.at(pos) == '-')
	{
		initialPos = cadena.find_first_not_of(delimitadores, pos);
		pos = cadena.find_first_of(delimitadores, initialPos);
	}	
}

// Tokeniza el caso especial del acronimo, iterando a traves de la palabra encontrado puntos
// y seguir avanzando hasta que se encuentre un delimitador que no sea un punto
bool Tokenizador::TokenizarCasoEspecialAcronimo(string& cadena, string::size_type& initialPos, string::size_type& pos,
												int& length, const string& delimitadores) const
{
	bool tokeniza = false;

	while(string::npos != pos && cadena.at(pos) == '.' && pos+1 < length-1 &&
		  delimitadores.find(cadena[pos+1]) == string::npos)
	{
		tokeniza = true;
		initialPos = cadena.find_first_not_of(delimitadores, pos);
		pos = cadena.find_first_of(delimitadores, initialPos);
	}

	if(string::npos != pos && cadena.at(pos) == ',' && pos+1 < length-1 &&
	   isdigit((int)cadena.at(pos-1)) && isdigit((int)cadena.at(pos+1)))
		TokenizarCasoEspecialDecimal(cadena, initialPos, pos, length, delimitadores);

	return tokeniza;
}

// Tokeniza el caso especial de emails, se dara el caso cuando se encuentre un simbolo arroba
// una vez encontrado y sabiendo que tenemos un numero o letra delante del arroba, buscamos los
// caracteres especificados que pueden aparecer en un correo y si encontramos un delimitador que no
// sea uno de ellos detenemos el avance
void Tokenizador::TokenizarCasoEspecialEmail(string& cadena, string::size_type& initialPos,
											 string::size_type& pos, const string& delimitadores) const
{
	bool leave = false;
	string::size_type posicion;
	string delimitadoresEspeciales = delimitadores+"._-";

	while(string::npos != pos && !leave)
	{
		posicion = cadena.find_first_of(delimitadoresEspeciales, pos+1);
		if(posicion == string::npos) leave = true;
		else if(posicion != string::npos && cadena.at(posicion) != '.' &&
		   cadena.at(posicion) != '-' && cadena.at(posicion) != '_')
			leave = true;
		else
		{
			initialPos = cadena.find_first_not_of(delimitadores, pos);
			pos = cadena.find_first_of(delimitadores, initialPos);
		}
	}
}

// Tokeniza el caso especial de URL, se dara el caso si se encuentra una palabra ftp:, http: o https:
// entonces seguira avanzando por la palabra hasta que encuentre un delimitador que no sea uno de los
// simbolos especificados que pueden aparecer en una URL
void Tokenizador::TokenizarCasoEspecialURL(string& cadena, string::size_type& initialPos,
										   string::size_type& pos, const string& delimitadores) const
{
	bool leave = false;
	while(string::npos != pos && !leave)
	{
		if(pos != string::npos && cadena.at(pos) != '_' && cadena.at(pos) != ':' &&
		   cadena.at(pos) != '.' && cadena.at(pos) != '?' && cadena.at(pos) != '&' &&
		   cadena.at(pos) != '-' && cadena.at(pos) != '=' && cadena.at(pos) != '#' &&
		   cadena.at(pos) != '@' && cadena.at(pos) != '/')
			leave = true;
		else
		{
			initialPos = cadena.find_first_not_of(delimitadores, pos);
			pos = cadena.find_first_of(delimitadores, initialPos);
		}
	}
}

// Tokeniza el fichero i guardando la salida en el fichero f (una palabra en cada linea del fichero).
// Devolvera true si se realiza la tokenizacion de forma correcta enviando a cerr el mensaje
// correspondiente (p.ej. que no exista el archivo i)
bool Tokenizador::Tokenizar(const string& inputFile, const string& outputFile) const
{
	ifstream i;
	ofstream f;
	string cadena;

	i.open(inputFile.c_str());
	f.open(outputFile.c_str());

	if(!i) {
		cerr << "ERROR: No existe el archivo: " << inputFile << "\n";
		return false;
	}
	else if(!f) {
		cerr << "ERROR: No existe el archivo de salida: " << outputFile << "\n";
		return false;
	}
	else
	{
		while(!i.eof())
		{
			cadena = "";
			getline(i, cadena);

			if(!cadena.empty())
				Tokenizar(cadena, f);

		}
	}
	i.close();
	f.close();

	return true;
}

// Tokeniza el fichero i guardando la salida en un fichero de nombre i anadiendole extension .tk 
// (sin eliminar previamente la extension de i), y que contendra una palabra en cada linea del fichero.
// Devolvera true si se realiza la tokenizacion de forma correcta enviando a cerr el mensaje
// correspondiente (p.ej. que no exista el archivo i)
bool Tokenizador::Tokenizar(const string& inputFile) const
{
	if(Tokenizar(inputFile, inputFile + ".tk"))
		return true;
	else
		return false;
}

// Tokeniza el fichero i guardando la salida en un fichero de nombre i anadiendole extension .tk 
// (sin eliminar previamente la extension de i), y que contendra una palabra en cada linea del fichero.
// Devolvera true si se realiza la tokenizacion de forma correcta enviando a cerr el mensaje
// correspondiente (p.ej. que no exista el archivo i)
bool Tokenizador::TokenizarListaIntermedia(const string& inputFile, list<string>& list) const
{
	ifstream i;
	ofstream f;
	string cadena;

	i.open(inputFile.c_str());

	if(!i) {
		cerr << "ERROR: No existe el archivo: " << inputFile << "\n";
		return false;
	}
	else
	{
		while(!i.eof())
		{
			cadena = "";
			getline(i, cadena);

			if(!cadena.empty())
				TokenizarEnLista(cadena, list);

		}
	}
	i.close();
	f.close();

	return true;
}

// Tokeniza el fichero i que contiene un nombre de fichero por linea guardando la salida en un fichero
// cuyo nombre sera el de entrada anadiendole extension .tk, y que contendra una palabra en cada linea
// del fichero. Devolvera true si se realiza la tokenizacion de forma correcta enviando a cerr el mensaje
// correspondiente (p.ej. que no exista el archivo i, o bien enviando a “cerr” los archivos de i que no existan)
bool Tokenizador::TokenizarListaFicheros(const string& inputFile) const
{
	ifstream i;
	string cadena;

	i.open(inputFile.c_str());

	if(!i) {
		cerr << "ERROR: No existe el archivo: " << inputFile << "\n";
		return false;
	}
	else
	{
		while(!i.eof())
		{
			cadena = "";
			getline(i, cadena);
			if(!cadena.empty()){
				if(!Tokenizar(cadena))
					return false;
			}
		}
	}
	i.close();
	return true;
}

// Tokeniza todos los archivos que contenga el directorio i, incluyendo los de los subdirectorios,
// guardando la salida en ficheros cuyo nombre sera el de entrada anadiendole extension .tk, y
// que contendra una palabra en cada linea del fichero. Devolvera true si se realiza la tokenizacion
// de forma correcta enviando a cerr el mensaje correspondiente (p.ej. que no exista el directorio i)
bool Tokenizador::TokenizarDirectorio(const string& inputDirectory) const
{
	struct stat dir;

	// Compruebo la existencia del directorio
 	int err=stat(inputDirectory.c_str(), &dir);

  	if(err==-1 || !S_ISDIR(dir.st_mode))
  		return false;

  	else {
		// Hago una lista en un fichero con find>fich
		string cmd="find "+inputDirectory+" -follow -type f |sort > .lista_fich"; 
		system(cmd.c_str());
		return TokenizarListaFicheros(".lista_fich");
	}
}

// Si casosEspeciales no esta a true, se tokenizara buscando delimitadores y tokenizando
// las palabras que dejan tras ellos (no anteriormente tokenizadas)
void Tokenizador::TokenizarSinCasosEspeciales(const string& str, ofstream& f) const
{
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while(string::npos != pos || string::npos != lastPos)
	{
		f << str.substr(lastPos, pos - lastPos) << "\n";
		lastPos = str.find_first_not_of(delimiters, pos);
		pos = str.find_first_of(delimiters, lastPos);
	}
}

void Tokenizador::TokenizarEnLista(const string& str, list<string>& lista) const
{
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while(string::npos != pos || string::npos != lastPos)
	{
		lista.push_back(str.substr(lastPos, pos - lastPos));
		lastPos = str.find_first_not_of(delimiters, pos);
		pos = str.find_first_of(delimiters, lastPos);
	}
}

void Tokenizador::TokenizarPregunta(const string& str, vector<string>& termPreg)
{
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while(string::npos != pos || string::npos != lastPos)
	{
		termPreg.push_back(str.substr(lastPos, pos - lastPos));
		lastPos = str.find_first_not_of(delimiters, pos);
		pos = str.find_first_of(delimiters, lastPos);
	}
}

// Si pasarAminuscSinAcentos esta a true, se eliminara los acentos y se
// pasara a minusculas tanto los valores obtenidos como las letras en mayusculas 
string Tokenizador::PasarAMinusculasSinAcentos(const string& str) const
{
	wstring normalCharacters = L"aaaaaaÆceeeeiiiiÐñooooo×ØuuuuÝÞßaaaaaaæceeeeiiiiðñooooo÷øuuuu";

	string cadena = str;

	string::iterator it;

	for(it = cadena.begin(); it!=cadena.end(); ++it)
	{
		if(*it+256 >= 192 && *it+256 <= 252)
		{
			*it = normalCharacters[256+*it -192];
		}

		if(*it <= 'Z' && *it >= 'A')
			*it -= ('Z' - 'z');
	}


	return cadena;

}

void Tokenizador::Guardar(ofstream& i) const
{
	i << delimiters << "\n";
	i << casosEspeciales << "\n";
	i << pasarAminuscSinAcentos << "\n";
}

void Tokenizador::RecuperarInformacion(ifstream& i)
{
	getline(i, delimiters);
	getline(i, delimiters);
	i >> casosEspeciales;
	i >> pasarAminuscSinAcentos;
}