#include <iostream> 
#include <string>
#include <list> 
#include <sys/resource.h>
#include "indexadorHash.h"

using namespace std;

double getcputime(void) {
struct timeval tim;
struct rusage ru;
getrusage(RUSAGE_SELF, &ru);
tim=ru.ru_utime;
double t=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
tim=ru.ru_stime;
t+=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
return t;
}

int
main(void)
{
	IndexadorHash b("./StopWordsEspanyol.txt", ". ,:", false, false, "./indicePrueba", 0, false, true);

	double i = getcputime();
	b.IndexarDirectorio ("corpus");
	double f = getcputime();

	cout << "Indexar: " << (f-i) << endl;

	i = getcputime();
	b.GuardarIndexacion();
	f = getcputime();

	cout << "Guardar: " << (f-i) << endl;
}
