#include "indexadorHash.h"

ostream & operator<<(ostream& s, const IndexadorHash& p)
{
	s << "Fichero con el listado de palabras de parada: " << p.ficheroStopWords << "\n";
	s << "Tokenizador: " << p.tok << "\n";
	s << "Directorio donde se almacenara el indice generado: " << p.directorioIndice << "\n";
	s << "Stemmer utilizado: " << p.tipoStemmer << "\n";
	s << "Informacion de la coleccion indexada: " << p.informacionColeccionDocs << "\n";
	s << "Se almacenara parte del indice en disco duro: " << p.almacenarEnDisco << "\n";
	s << "Se almacenaran las posiciones de los terminos: " << p.almacenarPosTerm << "\n";

	return s;
}

IndexadorHash::IndexadorHash(const string& fichStopWords, const string& delimitadores,
							 const bool& detectComp, const bool& minuscSinAcentos,
							 const string& dirIndice, const int& tStemmer,
							 const bool& almEnDisco, const bool& almPosTerm)
{
	ficheroStopWords = fichStopWords;
	tok.DelimitadoresPalabra(delimitadores);
	tok.CasosEspeciales(detectComp);
	tok.PasarAminuscSinAcentos(minuscSinAcentos);
	directorioIndice = dirIndice;
	tipoStemmer = tStemmer;
	almacenarEnDisco = almEnDisco;
	almacenarPosTerm = almPosTerm;
	indice.clear();
	indiceDocs.clear();
	informacionColeccionDocs.~InfColeccionDocs();

	AlmacenarStopWords();
}

IndexadorHash::IndexadorHash(const string& directorioIndexacion)
{
	ifstream i;
	i.open("./" + directorioIndexacion + "/indexador");
	RecuperarInformacionIndice(i);
	RecuperarInformacionIndiceDocs(i);
	informacionColeccionDocs.RecuperarInformacion(i);
	RecuperarInformacionDatosPrimitivos(i);
	tok.RecuperarInformacion(i);
	RecuperarPregunta(i);
	RecuperarInformacionStopWords(i);
}

IndexadorHash::IndexadorHash(const IndexadorHash& indexadorHash)
{
	indice = indexadorHash.getIndice();
	indiceDocs = indexadorHash.getIndiceDocs();
	informacionColeccionDocs = indexadorHash.getInformacionColeccionDocs();
	pregunta = indexadorHash.getPregunta();
	indicePregunta = indexadorHash.getIndicePregunta();
	infPregunta = indexadorHash.getInfPregunta();
	stopWords = indexadorHash.getStopWords();
	tok.DelimitadoresPalabra(indexadorHash.getTok().DelimitadoresPalabra());
	tok.CasosEspeciales(indexadorHash.getTok().CasosEspeciales());
	tok.PasarAminuscSinAcentos(indexadorHash.getTok().PasarAminuscSinAcentos());
	ficheroStopWords = indexadorHash.DevolverFichPalParada();
	directorioIndice = indexadorHash.DevolverDirIndice();
	tipoStemmer = indexadorHash.DevolverTipoStemming();
	almacenarEnDisco = indexadorHash.DevolverAlmEnDisco();
	almacenarPosTerm = indexadorHash.DevolverAlmacenarPosTerm();
}

IndexadorHash::IndexadorHash()
{
	ficheroStopWords = "";
	tok.DelimitadoresPalabra("");
	tok.CasosEspeciales(false);
	tok.PasarAminuscSinAcentos(false);
	directorioIndice = "";
	tipoStemmer = 0;
	almacenarEnDisco = false;
	almacenarPosTerm = false;
}

IndexadorHash::~IndexadorHash()
{
	ficheroStopWords = "";
	tok.DelimitadoresPalabra("");
	tok.CasosEspeciales(false);
	tok.PasarAminuscSinAcentos(false);
	directorioIndice = "";
	tipoStemmer = 0;
	almacenarEnDisco = false;
	almacenarPosTerm = false;
	informacionColeccionDocs.~InfColeccionDocs();
	indiceDocs.clear();
	indice.clear();
	indicePregunta.clear();
	infPregunta.~InformacionPregunta();
	stopWords.clear();
}

unordered_map<string, InformacionTermino> IndexadorHash::getIndice()const{ return indice; }
unordered_map<string, InfDoc> IndexadorHash::getIndiceDocs()const{ return indiceDocs; }
InfColeccionDocs IndexadorHash::getInformacionColeccionDocs()const{ return informacionColeccionDocs; }
string IndexadorHash::getPregunta()const{ return pregunta; }
unordered_map<string, InformacionTerminoPregunta> IndexadorHash::getIndicePregunta()const{ return indicePregunta; }
InformacionPregunta IndexadorHash::getInfPregunta()const{ return infPregunta; }
unordered_set<string> IndexadorHash::getStopWords()const{ return stopWords; }
Tokenizador IndexadorHash::getTok()const{ return tok; }

bool IndexadorHash::IndexarDirectorio(const string& dir)
{	
	struct stat dirStat;

	// Compruebo la existencia del directorio
 	int err=stat(dir.c_str(), &dirStat);

  	if(err==-1 || !S_ISDIR(dirStat.st_mode))
  		return false;

  	else {
		// Hago una lista en un fichero con find>fich
		string cmd="find "+dir+" -follow -type f |sort > .lista_fich"; 
		system(cmd.c_str());
		return Indexar(".lista_fich");
	}
}

bool IndexadorHash::Indexar(const string& ficheroDocumentos)
{
	ifstream i;
	string cadena;
	int doc = 1;
	i.open(ficheroDocumentos.c_str());

	if(!i)
	{
		cerr << "ERROR: No existe el archivo: " << ficheroDocumentos << "\n";
		return false;
	}
	else
	{
		while(!i.eof())
		{
			cadena = "";
			getline(i, cadena);

			if(!cadena.empty())
			{
				if(IndexarDocumento(cadena, doc))
				{
					indiceDocs[cadena].setIdDoc(doc);
					informacionColeccionDocs.setNumDocs(informacionColeccionDocs.getNumDocs()+1);
					informacionColeccionDocs.setNumTotalPal(informacionColeccionDocs.getNumTotalPal() + indiceDocs[cadena].getNumPal());
					informacionColeccionDocs.setNumTotalPalSinParada(informacionColeccionDocs.getNumTotalPalSinParada() + indiceDocs[cadena].getNumPalSinParada());
					informacionColeccionDocs.setTamBytes(informacionColeccionDocs.getTamBytes() + indiceDocs[cadena].getTamBytes());
					doc++;
				}
			}	

		}

		informacionColeccionDocs.setNumTotalPalDiferentes(indice.size());

		i.close();
	}

	return true;
}

bool IndexadorHash::IndexarDocumento(const string& documento, int id)
{
	ifstream i;
	ifstream j;
	ofstream f;
	string cadena;
	int pos = 0;

	i.open(documento.c_str());

	if(!i)
	{
		cerr << "ERROR: No existe el archivo: " << documento << "\n";
		return false;
	}
	else
	{
		
		stemmerPorter stemmer;
		
		// Calcular tamaño en bytes del documento y fecha
		struct stat docStat;
 		stat(documento.c_str(), &docStat);
		time_t fechaModificacion = docStat.st_mtime;


		if (indiceDocs.find(documento) != indiceDocs.end())
		{
			if (fechaModificacion > indiceDocs[documento].getFecha())
			{
				id = indiceDocs[documento].getIdDoc();
				informacionColeccionDocs.setNumDocs(informacionColeccionDocs.getNumDocs()-1);
				BorraDoc(documento);
			}
			else
				return false;
		}


		tok.Tokenizar(documento);
		
		j.open((documento + ".tk").c_str());


		while(!j.eof())
		{
			cadena = "";
			getline(j, cadena);

			if(!cadena.empty())
			{ 
				if(tipoStemmer != 0)
					stemmer.stemmer(cadena, tipoStemmer);

				if(stopWords.find(cadena) == stopWords.end())
					IndexarPalabra(cadena, documento, id, pos);
				
				indiceDocs[documento].setNumPal();
			}	
			pos++;
		}

		indiceDocs[documento].setFecha(fechaModificacion);
		indiceDocs[documento].setTamBytes(docStat.st_size);

		system(("rm " + documento + ".tk").c_str());
		i.close();
		j.close();

		return true;
	}
}

void IndexadorHash::IndexarPalabra(const string& cadena, const string& documento, int& id, int& pos)
{
	if(!Existe(cadena)){
		indiceDocs[documento].setNumPalDiferentes();
		indice[cadena].addLDocsPosition(id, pos, almacenarPosTerm);
	}
	else
	{
		if(indice[cadena].addLDocsPosition(id, pos, almacenarPosTerm))
			indiceDocs[documento].setNumPalDiferentes();
	}
	
	indice[cadena].setFtc(indice[cadena].getFtc()+1);
	
	indiceDocs[documento].setNumPalSinParada();
}

bool IndexadorHash::Devuelve(const string& word, InformacionTermino& info) const
{

	if(Existe(word))
	{
		info = indice.at(word);
		return true;
	}
	else
		return false; 
}

bool IndexadorHash::Devuelve(const string& word, const string& doc, InfTermDoc& info) const
{
	if(Existe(word))
	{
		if(indiceDocs.find(doc) != indiceDocs.end())
		{
			int id = indiceDocs.at(doc).getIdDoc();
			if(indice.at(word).getLDocs().find(id) != indice.at(word).getLDocs().end())
			{
				info = indice.at(word).getLDocs().at(id);
				return true;
			}		
		}
	}
	info.VaciarPosTerm();
	return false;
}

bool IndexadorHash::RecuperarIndexacion(const string& dir)
{
	struct stat docStat;
 	stat(dir.c_str(), &docStat);
	bool isdir = S_ISDIR(docStat.st_mode);

	// Si el directorio no existe, lo creamos
	if(isdir)
	{
		ifstream i;
		i.open("./"+ dir + "/indexador");

		if(i)
		{
			this->~IndexadorHash();
			RecuperarInformacionIndice(i);
			RecuperarInformacionIndiceDocs(i);
			informacionColeccionDocs.RecuperarInformacion(i);
			RecuperarInformacionDatosPrimitivos(i);
			tok.RecuperarInformacion(i);
			RecuperarPregunta(i);
			RecuperarInformacionStopWords(i);


			i.close();
			return true;
		}
	}

	return false;
}

void IndexadorHash::RecuperarInformacionIndice(ifstream& i)
{
	int size;
	i >> size;
	string palabra = "";
	InformacionTermino info;

	while(size != 0)
	{
		i >> palabra;
		info.RecuperarInformacion(i);
		indice[palabra] = info;
		info.VaciarLDocs();
		--size;
	}
}

void IndexadorHash::RecuperarInformacionIndiceDocs(ifstream& i)
{
	int size;
	i >> size;

	string ruta;
	InfDoc info;

	for(int j = 0; j < size; ++j)
	{
		i >> ruta;
		info.RecuperarInformacion(i);
		indiceDocs[ruta] = info;
	}
}

void IndexadorHash::RecuperarInformacionStopWords(ifstream& i)
{
	string stopWord;
	int size;
	i >> size;

	for(int j = 0; j < size; ++j)
	{
		i >> stopWord;
		stopWords.insert(stopWord);
	}
}

void IndexadorHash::RecuperarInformacionDatosPrimitivos(ifstream& i)
{
	i >> ficheroStopWords;
	i >> directorioIndice;
	i >> tipoStemmer;
	i >> almacenarEnDisco;
	i >> almacenarPosTerm;
}

void IndexadorHash::RecuperarPregunta(ifstream& i)
{
	i >> pregunta;
	infPregunta.RecuperarInformacion(i);

	int size;
	i >> size;

	string palabra;
	for(int j = 0; j < size; ++j)
	{
		i >> palabra;
		indicePregunta[palabra].RecuperarInformacion(i);
	}
}

void IndexadorHash::ImprimirIndexacion() const 
{
	cout << "Terminos indexados: " << "\n";
	for ( auto it = indice.begin(); it != indice.end(); ++it )
		cout << it->first << '\t' << it->second << "\n";

	cout << "Documentos indexados: " << "\n";
	for ( auto itDocs = indiceDocs.begin(); itDocs != indiceDocs.end(); ++itDocs )
		cout << itDocs->first << '\t' << itDocs->second << "\n";
}

bool IndexadorHash::GuardarIndexacion() const
{
	struct stat docStat;
 	stat(directorioIndice.c_str(), &docStat);
	bool isdir = S_ISDIR(docStat.st_mode);

	// Si el directorio no existe, lo creamos
	if(!isdir)
		system(("mkdir \"./" + directorioIndice + "\"").c_str());

	ofstream i;
	i.open("./" + directorioIndice + "/indexador");


	GuardarIndice(i);
	GuardarIndiceDocs(i);
	informacionColeccionDocs.Guardar(i);
	GuardarDatosPrimitivosIndexador(i);
	tok.Guardar(i);
	GuardarPregunta(i);
	GuardarStopWords(i);
	
	i.close();
	return true;
}

void IndexadorHash::GuardarIndice(ofstream& i) const
{
	i << indice.size() << "\n";

	auto it = indice.begin();
	while(it != indice.end())
	{
		i << it->first << "\n";
		it->second.Guardar(i);
		++it;
	}
}

void IndexadorHash::GuardarIndiceDocs(ofstream &i) const
{
	i << indiceDocs.size() << "\n";

	for ( auto it = indiceDocs.begin(); it != indiceDocs.end(); ++it )
	{
		i << it->first << "\n";
		it->second.Guardar(i);
	}
}

void IndexadorHash::GuardarStopWords(ofstream &i) const
{
	i << stopWords.size() << "\n";

	for(string stopWord: stopWords)
		i << stopWord << "\n";
}

void IndexadorHash::GuardarDatosPrimitivosIndexador(ofstream& i) const
{
	i << ficheroStopWords << "\n";
	i << directorioIndice << "\n";
	i << tipoStemmer << "\n";
	i << almacenarEnDisco << "\n";
	i << almacenarPosTerm << "\n";
}

void IndexadorHash::GuardarPregunta(ofstream& i) const
{
	i << pregunta << "\n";

	infPregunta.Guardar(i);

	i <<indicePregunta.size() << "\n";

	for(auto it = indicePregunta.begin(); it != indicePregunta.end(); ++it)
	{
		i << it->first << "\n";
		it->second.Guardar(i);
	}
}


void IndexadorHash::AlmacenarStopWords()
{
	ifstream i;
	ofstream f;
	string cadena;

	i.open(ficheroStopWords.c_str());

	if(!i)
		cerr << "ERROR: No existe el archivo: " << ficheroStopWords << "\n";
	else
	{
		while(!i.eof())
		{
			cadena = "";
			getline(i, cadena);

			if(!cadena.empty())
				stopWords.insert(cadena);
		}
	}

	i.close();
}


bool IndexadorHash::Existe(const string& word) const
{
	return (indice.find(word) != indice.end());
}

bool IndexadorHash::Actualiza(const string& word, const InformacionTermino& inf) 
{
	InformacionTermino temp;
	if(Existe(word))
	{
		indice[word] = inf;
		return true;
	}
	else
	{
		cerr << "ERROR: En actualiza no existe la palabra " << word << "\n";
		return false;
	}
}

bool IndexadorHash::Borra(const string& word)
{
	return (indice.erase(word) != 0);
}

bool IndexadorHash::BorraDoc(const string& doc)
{
	if(indiceDocs.find(doc) != indiceDocs.end())
	{
		unordered_map<string, InformacionTermino>::iterator it = indice.begin();

		while(it != indice.end())
		{
			int id = indiceDocs.at(doc).getIdDoc();
			
			if(it->second.getLDocs().find(id) != it->second.getLDocs().end())
			{
				it->second.BorrarDocumentoID(id);

				if(it->second.getFtc() == 0)
					it = indice.erase(it);
				else 
					++it;
			}
			else
				++it;
		}

		// Actualizar la informacion relacionada con la coleccion de docs
		informacionColeccionDocs.setNumDocs(informacionColeccionDocs.getNumDocs()-1);
		informacionColeccionDocs.setNumTotalPal(informacionColeccionDocs.getNumTotalPal()-indiceDocs.at(doc).getNumPal());
		informacionColeccionDocs.setNumTotalPalDiferentes(indice.size());
		informacionColeccionDocs.setNumTotalPalSinParada(informacionColeccionDocs.getNumTotalPalSinParada()-indiceDocs.at(doc).getNumPalSinParada());
		indiceDocs.erase(indiceDocs.find(doc));

		return true;
	}

	return false;
}

bool IndexadorHash::Inserta(const string& word, const InformacionTermino& info)
{
	if(Existe(word))
		return false;
	else
	{
		indice[word] = info;
		return true;
	}
}

bool IndexadorHash::IndexarPregunta(const string& preg)
{
	indicePregunta.clear();
	infPregunta.~InformacionPregunta();

	vector<string> terminos;
	tok.TokenizarPregunta(preg, terminos);
	
	stemmerPorter stemmer;

	int pos = 0;
	if(!terminos.empty())
	{
		for(string term: terminos)
		{
			if(tipoStemmer != 0)
				stemmer.stemmer(term, tipoStemmer+1);

			// Si la palabra encontrada no es una palabra de parada, indexamos
			if(stopWords.find(term) == stopWords.end())
				IndexarPalabraPregunta(term, pos);

			pos++;
		}

		infPregunta.setNumTotalPal(pos);
		infPregunta.setNumTotalPalDiferentes(indicePregunta.size());
		pregunta = preg;
		return true;
	}
	else return false;
}

void IndexadorHash::IndexarPalabraPregunta(const string& pal, int& pos)
{
	InformacionTerminoPregunta info;
	if(DevuelvePregunta(pal, info))
	{
		info.setFt(info.getFt()+1);
		info.addPreguntaPosTerm(pos);
	}
	else InformacionTerminoInicialPreg(info, pos);

	infPregunta.setNumTotalPalSinParada(infPregunta.getNumTotalPalSinParada()+1);

	indicePregunta[pal] = info;
}

bool IndexadorHash::DevuelvePregunta(const string& word, InformacionTerminoPregunta& info)
{
	if(indicePregunta.find(word) != indicePregunta.end())
	{
		info = indicePregunta[word];
		return true;
	}
	else { info.~InformacionTerminoPregunta(); return false; }
}

bool IndexadorHash::DevuelvePregunta(string& preg) const
{
	if(!pregunta.empty()){
		preg = pregunta;
		return true;
	}
	else return false;
}

bool IndexadorHash::DevuelvePregunta(InformacionPregunta& info) const
{
	if(!pregunta.empty()){
		info = infPregunta;
		return true;
	}
	else return false;
}

void IndexadorHash::ImprimirIndexacionPregunta() 
{
	cout << "Pregunta indexada: " << pregunta << "\n";
	cout << "Terminos indexados en la pregunta: " << "\n";

	auto it = indicePregunta.begin();
	while(it != indicePregunta.end())
		cout << it->first << '\t' << it->second << "\n";

	cout << "Informacion de la pregunta: " << infPregunta << "\n"; 
}

void IndexadorHash::InformacionTerminoInicialPreg(InformacionTerminoPregunta& info, int& pos)
{
	info.setFt(1);
	info.addPreguntaPosTerm(pos);
}

void IndexadorHash::ImprimirPregunta()
{
	cout << "Pregunta indexada: " << pregunta << "\n";
	cout << "Información de la pregunta: " << infPregunta << "\n";
}

void IndexadorHash::ListarPalParada() const
{
	for(auto it = stopWords.begin(); it != stopWords.end(); ++it)
		cout << *it << "\n";
}

void IndexadorHash::ListarInfColeccDocs() const
{
	cout << informacionColeccionDocs << "\n";
}

void IndexadorHash::ListarDocs() const
{
	for ( auto it = indiceDocs.begin(); it != indiceDocs.end(); ++it )
		cout << it->first << "\t" << it->second << "\n";
}

bool IndexadorHash::ListarDocs(const string& doc) const
{
	unordered_map<string,InfDoc>::const_iterator it = indiceDocs.find(doc);
	if(it != indiceDocs.end())
	{
		cout << it->first << "\t" << it->second << "\n";

		return true;
	}
	else return false;
}

void IndexadorHash::ListarTerminos() const
{
	for ( auto it = indice.begin(); it != indice.end(); ++it )
		cout << it->first << '\t' << it->second << "\n";
}

bool IndexadorHash::ListarTerminos(const string& doc) const
{
	auto it = indiceDocs.find(doc);
	if(it != indiceDocs.end())
	{
		if(it->second.getNumPal() != 0)
		{
			int id = it->second.getIdDoc();
			for ( auto it = indice.begin(); it != indice.end(); ++it )
			{
				if(it->second.getLDocs().find(id) != it->second.getLDocs().end())
					cout << it->first << '\t' << it->second << "\n";
			}
		}
		return true;
	}
	else return false;
}

int IndexadorHash::NumPalParada() const
{
	return stopWords.size();
}

void IndexadorHash::VaciarIndice()
{
	indice.clear();
	indiceDocs.clear();
	indicePregunta.clear();
}

int IndexadorHash::NumPalIndexadas() const
{
	return indice.size();
}

Tokenizador IndexadorHash::DevolverTokenizador() const{ return tok; }
string IndexadorHash::DevolverFichPalParada() const{ return ficheroStopWords; }
string IndexadorHash::DevolverDelimitadores() const{ return tok.DelimitadoresPalabra(); }
bool IndexadorHash::DevolverCasosEspeciales() const{ return tok.CasosEspeciales(); }
bool IndexadorHash::DevolverPasarAminuscSinAcentos() const{ return tok.PasarAminuscSinAcentos(); }
bool IndexadorHash::DevolverAlmacenarPosTerm() const{ return almacenarPosTerm; }
string IndexadorHash::DevolverDirIndice() const{ return directorioIndice; }
int IndexadorHash::DevolverTipoStemming() const{ return tipoStemmer; }
bool IndexadorHash::DevolverAlmEnDisco() const{ return almacenarEnDisco; }